#!/usr/bin/python


import argparse
import sys
import os
import shutil
import fileinput

from src import html_helper
from src import file_walker
from shutil import copytree

DATA_PATH = 'Results/'
SHOW_REMAININGL_FIGS = True
SHOW_NOT_FOUND = True


parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--eosPass", default=None, help='Password to acces ' + file_walker.EOS_PREFIX + DATA_PATH)
parser.add_argument("--eosUser", default='hgsensor', help='Username to acces ' + file_walker.EOS_PREFIX + DATA_PATH)
args = parser.parse_args()


def test_category(name, cat, catList=[]):
    if (name.startswith(cat)):
        for item in catList:
            if item == cat:
                continue
            if (name.startswith(item)) and len(item) > len(cat):
                return False
        return True
    return False

def displayFigure(name, caption, filename):
    printFig = "{{< img src=\"/" + name + "\" caption=\"" + caption + "\" >}}"
    print(printFig, file=filename)
    print('', file=filename)
    return True

def displayPDF(name, caption, filename):
#    #converting pdf into jpg
#    name_jpg = name.replace('.pdf','_convert_to_jpg.jpg')
#    convertCommand = "convert -quality 100 " + "static/images/" + name + " " + "static/images/" + name_jpg
#    print("Converting pdf in jpg ... ")
#    os.system(convertCommand)
#    displayFigure(name_jpg, caption, filename)

    printPdf = "<iframe width=\"400\" height=\"400\" src=\"/" + name + "\"></iframe>"
    print(printPdf, file=filename)
    print('', file=filename)

    printCapt = "[" + caption + "](/" + name + ")"
    print(printCapt, file=filename)
    print('', file=filename)

    return True

dataDirs, dataCategories, dataSubDirContent = file_walker.walk_dirs(DATA_PATH, eosPass=args.eosPass, eosUser=args.eosUser, verbose=1)


#commented line to not create the index of sensorslist anymore
listDir = 'sensorslist/'
#HugoCommandNewFile = " hugo new " + listDir + "_index.md"
#print(HugoCommandNewFile)
#os.system(HugoCommandNewFile)

mainDir = 'content/' + listDir
listName = mainDir + '_index.md'
print(listName)
#ERICA:: It doesnt change the title, yet!
#for line in open(listName):
#    if 'Sensorslist' in line:
#        line = line.replace('Sensorslist','Sensors list')
#    sys.stdout.write(line)
#
#with open(listName, 'a') as myfile:
#    print('The following sensor data collections are available:', file=myfile)
#    print('', file=myfile)
#    print('{{%children %}}', file=myfile)

for dataSubDir, dataSubDirName, dataSubFileList in dataSubDirContent:
    #print(dataSubDir)
    HugoCommandNewDataFile = "hugo new " + listDir + dataSubDir + "/_index.md"
    #print(HugoCommandNewDataFile)
    os.system(HugoCommandNewDataFile)
    dataName = mainDir + dataSubDir + "/_index.md"
    with open(dataName, 'a') as myfile:
        print('EOS path:', file_walker.EOS_PREFIX + DATA_PATH + dataSubDir, file=myfile)
        print('', file=myfile)
        print('The following folders are available:', file=myfile)
        print('', file=myfile)
        print('{{%children %}}', file=myfile)
        print('', file=myfile)

        print('Now create the single figures (only jpg, png and pdf are considered) ...')
        #IV and CV figures
        hexPlots = [plot for plot in dataSubFileList
                    if 'IV.jpg' in plot or 'CV.jpg' in plot
                    or 'IV.png' in plot or 'CV.png' in plot
                    or 'IV.pdf' in plot or 'CV.pdf' in plot]
        hexPlots = sorted(hexPlots)
        #print(hexPlots)
        print("<center>", file=myfile)
        if hexPlots or SHOW_NOT_FOUND:
            print('## HexPlots', file=myfile)
            if not hexPlots:
                print("No IV or CV plots found in this folder.", file=myfile)
            for plot in hexPlots:
                srcpath, srcfile = os.path.split(plot)
                #os.system(commandCopy)
                if '.jpg' in plot:
                    displayFigure(plot, srcfile, myfile)
                if '.png' in plot:
                    displayFigure(plot, srcfile, myfile)
                if '.pdf' in plot:
                    displayPDF(plot, srcfile, myfile)

        # total currents
        totPlots = [plot for plot in dataSubFileList
                    if any(x in plot for x in ['IV_totcurrent.jpg', 'CV_totcurrent.jpg',
                    'IV_totcurrent.pdf', 'CV_totcurrent.pdf', 'IV_totcurrent.png', 'CV_totcurrent.png'])]
        totPlots = sorted(totPlots)
        #print(totPlots)
        if totPlots or SHOW_NOT_FOUND:
            print('## Total currents', file=myfile)
            if not totPlots:
                print("No total current plots found in this folder.", file=myfile)
            for plot in totPlots:
                srcpath, srcfile = os.path.split(plot)
                if '.jpg' in plot:
                    displayFigure(plot, srcfile, myfile)
                if '.png' in plot:
                    displayFigure(plot, srcfile, myfile)
                if '.pdf' in plot:
                    displayPDF(plot, srcfile, myfile)

        # other plots
        if SHOW_REMAININGL_FIGS:
            otherPlots = [plot for plot in dataSubFileList
                          if '.pdf' in plot and plot not in hexPlots
                          or '.jpg' in plot and plot not in hexPlots
                          or '.png' in plot and plot not in hexPlots]
            otherPlots = sorted(otherPlots)
            #print(otherPlots)
            if otherPlots or SHOW_NOT_FOUND:
                print('## Other plots', file=myfile)
                if not otherPlots:
                    print("No other plots found in this folder.", file=myfile)
                for plot in otherPlots:
                    srcpath, srcfile = os.path.split(plot)
                    if '.jpg' in plot:
                        displayFigure(plot, srcfile, myfile)
                    if '.png' in plot:
                        displayFigure(plot, srcfile, myfile)
                    if '.pdf' in plot:
                        displayPDF(plot, srcfile, myfile)

        print("</center>", file=myfile)

print("Done!")
