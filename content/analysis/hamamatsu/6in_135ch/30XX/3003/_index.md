---
title: "3003"
date: 2019-01-18T14:39:26+01:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_Hamamatsu_3003.pdf](/images/Hamamatsu/6in_135ch/30XX/3003/IV_map_Hamamatsu_3003.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/30XX/3003/IV_map_Hamamatsu_3003.png" caption="IV_map_Hamamatsu_3003.png" >}}

[IV_selCh_Hamamatsu_3003.pdf](/images/Hamamatsu/6in_135ch/30XX/3003/IV_selCh_Hamamatsu_3003.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/30XX/3003/IV_selCh_Hamamatsu_3003.png" caption="IV_selCh_Hamamatsu_3003.png" >}}

[IV_selCh_Hamamatsu_3003_stats.pdf](/images/Hamamatsu/6in_135ch/30XX/3003/IV_selCh_Hamamatsu_3003_stats.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/30XX/3003/IV_selCh_Hamamatsu_3003_stats.png" caption="IV_selCh_Hamamatsu_3003_stats.png" >}}

</center>
