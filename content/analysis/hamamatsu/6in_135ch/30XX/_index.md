---
title: "30XX"
date: 2019-01-18T14:39:26+01:00
weight: 2
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_selCh_means_summary.pdf](/images/Hamamatsu/6in_135ch/30XX/IV_selCh_means_summary.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/30XX/IV_selCh_means_summary.png" caption="IV_selCh_means_summary.png" >}}

[IV_selCh_medians_summary.pdf](/images/Hamamatsu/6in_135ch/30XX/IV_selCh_medians_summary.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/30XX/IV_selCh_medians_summary.png" caption="IV_selCh_medians_summary.png" >}}

</center>
