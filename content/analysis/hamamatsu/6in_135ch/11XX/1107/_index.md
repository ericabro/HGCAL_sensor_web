---
title: "1107"
date: 2019-01-18T14:39:25+01:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_Hamamatsu_1107.pdf](/images/Hamamatsu/6in_135ch/11XX/1107/IV_map_Hamamatsu_1107.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/11XX/1107/IV_map_Hamamatsu_1107.png" caption="IV_map_Hamamatsu_1107.png" >}}

[IV_selCh_Hamamatsu_1107.pdf](/images/Hamamatsu/6in_135ch/11XX/1107/IV_selCh_Hamamatsu_1107.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/11XX/1107/IV_selCh_Hamamatsu_1107.png" caption="IV_selCh_Hamamatsu_1107.png" >}}

[IV_selCh_Hamamatsu_1107_stats.pdf](/images/Hamamatsu/6in_135ch/11XX/1107/IV_selCh_Hamamatsu_1107_stats.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/11XX/1107/IV_selCh_Hamamatsu_1107_stats.png" caption="IV_selCh_Hamamatsu_1107_stats.png" >}}

</center>
