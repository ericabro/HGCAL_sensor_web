---
title: "1106"
date: 2019-01-18T14:39:25+01:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_Hamamatsu_1106.pdf](/images/Hamamatsu/6in_135ch/11XX/1106/IV_map_Hamamatsu_1106.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/11XX/1106/IV_map_Hamamatsu_1106.png" caption="IV_map_Hamamatsu_1106.png" >}}

[IV_selCh_Hamamatsu_1106.pdf](/images/Hamamatsu/6in_135ch/11XX/1106/IV_selCh_Hamamatsu_1106.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/11XX/1106/IV_selCh_Hamamatsu_1106.png" caption="IV_selCh_Hamamatsu_1106.png" >}}

[IV_selCh_Hamamatsu_1106_stats.pdf](/images/Hamamatsu/6in_135ch/11XX/1106/IV_selCh_Hamamatsu_1106_stats.pdf)

{{< img src="/images/Hamamatsu/6in_135ch/11XX/1106/IV_selCh_Hamamatsu_1106_stats.png" caption="IV_selCh_Hamamatsu_1106_stats.png" >}}

</center>
