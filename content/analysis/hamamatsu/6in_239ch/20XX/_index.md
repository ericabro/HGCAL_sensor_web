---
title: "20XX"
date: 2019-01-18T14:39:24+01:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_selCh_means_summary.pdf](/images/Hamamatsu/6in_239ch/20XX/IV_selCh_means_summary.pdf)

{{< img src="/images/Hamamatsu/6in_239ch/20XX/IV_selCh_means_summary.png" caption="IV_selCh_means_summary.png" >}}

[IV_selCh_medians_summary.pdf](/images/Hamamatsu/6in_239ch/20XX/IV_selCh_medians_summary.pdf)

{{< img src="/images/Hamamatsu/6in_239ch/20XX/IV_selCh_medians_summary.png" caption="IV_selCh_medians_summary.png" >}}

</center>
