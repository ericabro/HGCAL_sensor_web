---
title: "2028"
date: 2019-01-18T14:39:24+01:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_Hamamatsu_2028.pdf](/images/Hamamatsu/6in_239ch/20XX/2028/IV_map_Hamamatsu_2028.pdf)

{{< img src="/images/Hamamatsu/6in_239ch/20XX/2028/IV_map_Hamamatsu_2028.png" caption="IV_map_Hamamatsu_2028.png" >}}

[IV_selCh_Hamamatsu_2028.pdf](/images/Hamamatsu/6in_239ch/20XX/2028/IV_selCh_Hamamatsu_2028.pdf)

{{< img src="/images/Hamamatsu/6in_239ch/20XX/2028/IV_selCh_Hamamatsu_2028.png" caption="IV_selCh_Hamamatsu_2028.png" >}}

[IV_selCh_Hamamatsu_2028_stats.pdf](/images/Hamamatsu/6in_239ch/20XX/2028/IV_selCh_Hamamatsu_2028_stats.pdf)

{{< img src="/images/Hamamatsu/6in_239ch/20XX/2028/IV_selCh_Hamamatsu_2028_stats.png" caption="IV_selCh_Hamamatsu_2028_stats.png" >}}

</center>
