---
title: "LogScale"
date: 2019-02-14T15:13:45+01:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[Hamamatsu_Z3414-No2_IV_log.pdf](/images/Hamamatsu/8in_198ch/300um/Z3414-No2/logScale/Hamamatsu_Z3414-No2_IV_log.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/300um/Z3414-No2/logScale/Hamamatsu_Z3414-No2_IV_log.png" caption="Hamamatsu_Z3414-No2_IV_log.png" >}}

[Hamamatsu_Z3414-No2_IV_selCh_log.pdf](/images/Hamamatsu/8in_198ch/300um/Z3414-No2/logScale/Hamamatsu_Z3414-No2_IV_selCh_log.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/300um/Z3414-No2/logScale/Hamamatsu_Z3414-No2_IV_selCh_log.png" caption="Hamamatsu_Z3414-No2_IV_selCh_log.png" >}}

[Hamamatsu_Z3414-No2_IV_selCh_log_stats.pdf](/images/Hamamatsu/8in_198ch/300um/Z3414-No2/logScale/Hamamatsu_Z3414-No2_IV_selCh_log_stats.pdf)

{{< img src="/images/Hamamatsu/8in_198ch/300um/Z3414-No2/logScale/Hamamatsu_Z3414-No2_IV_selCh_log_stats.png" caption="Hamamatsu_Z3414-No2_IV_selCh_log_stats.png" >}}

</center>
