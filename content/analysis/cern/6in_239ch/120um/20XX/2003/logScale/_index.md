---
title: "LogScale"
date: 2019-06-25T11:55:11+02:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[HPK_6in_239ch_120um_n_2003_IV_1000V.pdf](/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_1000V.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_1000V.png" caption="HPK_6in_239ch_120um_n_2003_IV_1000V.png" >}}

[HPK_6in_239ch_120um_n_2003_IV_100V.pdf](/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_100V.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_100V.png" caption="HPK_6in_239ch_120um_n_2003_IV_100V.png" >}}

[HPK_6in_239ch_120um_n_2003_IV_200V.pdf](/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_200V.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_200V.png" caption="HPK_6in_239ch_120um_n_2003_IV_200V.png" >}}

[HPK_6in_239ch_120um_n_2003_IV_300V.pdf](/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_300V.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_300V.png" caption="HPK_6in_239ch_120um_n_2003_IV_300V.png" >}}

[HPK_6in_239ch_120um_n_2003_IV_500V.pdf](/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_500V.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_500V.png" caption="HPK_6in_239ch_120um_n_2003_IV_500V.png" >}}

[HPK_6in_239ch_120um_n_2003_IV_800V.pdf](/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_800V.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_800V.png" caption="HPK_6in_239ch_120um_n_2003_IV_800V.png" >}}

[HPK_6in_239ch_120um_n_2003_IV_selCh.pdf](/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_selCh.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_selCh.png" caption="HPK_6in_239ch_120um_n_2003_IV_selCh.png" >}}

[HPK_6in_239ch_120um_n_2003_IV_selCh_stats.pdf](/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_selCh_stats.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_selCh_stats.png" caption="HPK_6in_239ch_120um_n_2003_IV_selCh_stats.png" >}}

[HPK_6in_239ch_120um_n_2003_IV_totcurr.pdf](/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_totcurr.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_totcurr.png" caption="HPK_6in_239ch_120um_n_2003_IV_totcurr.png" >}}

[HPK_6in_239ch_120um_n_2003_IV_totcurr_stats.pdf](/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_totcurr_stats.pdf)

{{< img src="/images/Analysis_IVmeas/6in_239ch/120um/20XX/2003/logScale/HPK_6in_239ch_120um_n_2003_IV_totcurr_stats.png" caption="HPK_6in_239ch_120um_n_2003_IV_totcurr_stats.png" >}}

</center>
