---
title: "6in_239ch"
date: 2019-06-25T11:55:11+02:00
draft: false
---

The following sensor comparison and plots are available:

{{%children %}}

The raw data can be found in:

{{%attachments style="orange" title="n-type" pattern="HPK_6in_239ch_120um_n.*(txt)" /%}}

{{%attachments style="green" title="p-type common" pattern="HPK_6in_239ch_120um_pcom.*(txt)" /%}}

{{%attachments style="blue" title="p-type individual" pattern="HPK_6in_239ch_120um_pind.*(txt)" /%}}
