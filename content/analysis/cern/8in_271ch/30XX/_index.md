---
title: "30XX"
date: 2018-08-15T14:06:33+02:00
draft: false
weight: 1
---

The following folders are available:

{{%children %}}

<center>
## HexPlots
[all_means_summary.pdf](/images/Analysis_IVmeas/8in_271ch/30XX/all_means_summary.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/30XX/all_means_summary.png" caption="all_means_summary.png" >}}

[all_medians_summary.pdf](/images/Analysis_IVmeas/8in_271ch/30XX/all_medians_summary.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/30XX/all_medians_summary.png" caption="all_medians_summary.png" >}}

</center>
