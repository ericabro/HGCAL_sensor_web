---
title: "3008"
date: 2018-08-15T14:06:33+02:00
draft: false
weight: 3008
---

EOS path: Analysis_IVmeas/8in_271ch/30XX/3008

The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_3008.pdf](/images/Analysis_IVmeas/8in_271ch/30XX/3008/IV_map_3008.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/30XX/3008/IV_map_3008.png" caption="IV_map_3008.png" >}}

[IV_map_3008_details_all_heat.pdf](/images/Analysis_IVmeas/8in_271ch/30XX/3008/IV_map_3008_details_all_heat.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/30XX/3008/IV_map_3008_details_all_heat.png" caption="IV_map_3008_details_all_heat.png" >}}

[IV_selCh_3008.pdf](/images/Analysis_IVmeas/8in_271ch/30XX/3008/IV_selCh_3008.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/30XX/3008/IV_selCh_3008.png" caption="IV_selCh_3008.png" >}}

[IV_selCh_3008_stats.pdf](/images/Analysis_IVmeas/8in_271ch/30XX/3008/IV_selCh_3008_stats.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/30XX/3008/IV_selCh_3008_stats.png" caption="IV_selCh_3008_stats.png" >}}

</center>
