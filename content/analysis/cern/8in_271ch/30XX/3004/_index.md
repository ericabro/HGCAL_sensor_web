---
title: "3004"
date: 2018-08-15T14:06:34+02:00
draft: false
weight: 3004
---

EOS path: Analysis_IVmeas/8in_271ch/30XX/3004

The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_3004.pdf](/images/Analysis_IVmeas/8in_271ch/30XX/3004/IV_map_3004.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/30XX/3004/IV_map_3004.png" caption="IV_map_3004.png" >}}

[IV_map_3004_details_all_heat.pdf](/images/Analysis_IVmeas/8in_271ch/30XX/3004/IV_map_3004_details_all_heat.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/30XX/3004/IV_map_3004_details_all_heat.png" caption="IV_map_3004_details_all_heat.png" >}}

[IV_selCh_3004.pdf](/images/Analysis_IVmeas/8in_271ch/30XX/3004/IV_selCh_3004.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/30XX/3004/IV_selCh_3004.png" caption="IV_selCh_3004.png" >}}

[IV_selCh_3004_stats.pdf](/images/Analysis_IVmeas/8in_271ch/30XX/3004/IV_selCh_3004_stats.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/30XX/3004/IV_selCh_3004_stats.png" caption="IV_selCh_3004_stats.png" >}}

</center>
