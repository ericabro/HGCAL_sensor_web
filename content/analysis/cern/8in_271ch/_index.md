---
title: "8in_271ch"
date: 2018-08-15T14:06:33+02:00
draft: false
---

The following sensor comparison and plots are available:

{{%children %}}

The raw data can be found in:

{{%attachments style="orange" title="120um" pattern="HPK_8in_271ch_3.*(txt)" /%}}

{{%attachments style="green" title="300um" pattern="HPK_8in_271ch_4.*(txt)" /%}}

{{%attachments style="blue" title="200um" pattern="HPK_8in_271ch_5.*(txt)" /%}}
