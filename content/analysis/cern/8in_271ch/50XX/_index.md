---
title: "50XX"
date: 2018-08-15T14:06:34+02:00
draft: false
weigth: 3
---

The following folders are available:

{{%children %}}

<center>
## HexPlots
[all_means_summary.pdf](/images/Analysis_IVmeas/8in_271ch/50XX/all_means_summary.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/50XX/all_means_summary.png" caption="all_means_summary.png" >}}

[all_medians_summary.pdf](/images/Analysis_IVmeas/8in_271ch/50XX/all_medians_summary.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/50XX/all_medians_summary.png" caption="all_medians_summary.png" >}}

</center>
