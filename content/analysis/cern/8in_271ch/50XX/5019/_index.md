---
title: "5019"
date: 2018-08-15T14:06:34+02:00
draft: false
---

EOS path: Analysis_IVmeas/8in_271ch/50XX/5019

The following folders are available:

{{%children %}}

<center>
## HexPlots
{{< img src="/images/Analysis_IVmeas/8in_271ch/50XX/5019/IV_map_5019.png" caption="IV_map_5019.png" >}}

[IV_map_5019_details_all_heat.pdf](/images/Analysis_IVmeas/8in_271ch/50XX/5019/IV_map_5019_details_all_heat.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/50XX/5019/IV_map_5019_details_all_heat.png" caption="IV_map_5019_details_all_heat.png" >}}

[all_HPK_8in_271ch_5019_IV_selChan.pdf](/images/Analysis_IVmeas/8in_271ch/50XX/5019/all_HPK_8in_271ch_5019_IV_selChan.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/50XX/5019/all_HPK_8in_271ch_5019_IV_selChan.png" caption="all_HPK_8in_271ch_5019_IV_selChan.png" >}}

[all_HPK_8in_271ch_5019_IV_selChan_stats.pdf](/images/Analysis_IVmeas/8in_271ch/50XX/5019/all_HPK_8in_271ch_5019_IV_selChan_stats.pdf)

{{< img src="/images/Analysis_IVmeas/8in_271ch/50XX/5019/all_HPK_8in_271ch_5019_IV_selChan_stats.png" caption="all_HPK_8in_271ch_5019_IV_selChan_stats.png" >}}

</center>
