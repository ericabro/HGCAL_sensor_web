---
title: "1109"
date: 2018-08-15T14:06:34+02:00
draft: false
---

EOS path: Analysis_IVmeas/6in_135ch/11XX/1109

The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_1109.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1109/IV_map_1109.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1109/IV_map_1109.png" caption="IV_map_1109.png" >}}

[IV_map_1109_details_all_heat.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1109/IV_map_1109_details_all_heat.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1109/IV_map_1109_details_all_heat.png" caption="IV_map_1109_details_all_heat.png" >}}

[IV_selCh_1109.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1109/IV_selCh_1109.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1109/IV_selCh_1109.png" caption="IV_selCh_1109.png" >}}

[IV_selCh_1109_stats.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1109/IV_selCh_1109_stats.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1109/IV_selCh_1109_stats.png" caption="IV_selCh_1109_stats.png" >}}

</center>
