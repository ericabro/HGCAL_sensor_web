---
title: "1107"
date: 2018-08-15T14:06:34+02:00
draft: false
---

EOS path: Analysis_IVmeas/6in_135ch/11XX/1107

The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_1107.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1107/IV_map_1107.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1107/IV_map_1107.png" caption="IV_map_1107.png" >}}

[IV_map_1107_details_all_heat.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1107/IV_map_1107_details_all_heat.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1107/IV_map_1107_details_all_heat.png" caption="IV_map_1107_details_all_heat.png" >}}

[IV_selCh_1107.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1107/IV_selCh_1107.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1107/IV_selCh_1107.png" caption="IV_selCh_1107.png" >}}

[IV_selCh_1107_stats.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1107/IV_selCh_1107_stats.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1107/IV_selCh_1107_stats.png" caption="IV_selCh_1107_stats.png" >}}

</center>
