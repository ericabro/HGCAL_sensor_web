---
title: "1110"
date: 2018-08-15T14:06:34+02:00
draft: false
---

EOS path: Analysis_IVmeas/6in_135ch/11XX/1110

The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_1110.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1110/IV_map_1110.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1110/IV_map_1110.png" caption="IV_map_1110.png" >}}

[IV_map_1110_details_all_heat.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1110/IV_map_1110_details_all_heat.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1110/IV_map_1110_details_all_heat.png" caption="IV_map_1110_details_all_heat.png" >}}

[IV_selCh_1110.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1110/IV_selCh_1110.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1110/IV_selCh_1110.png" caption="IV_selCh_1110.png" >}}

[IV_selCh_1110_stats.pdf](/images/Analysis_IVmeas/6in_135ch/11XX/1110/IV_selCh_1110_stats.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/11XX/1110/IV_selCh_1110_stats.png" caption="IV_selCh_1110_stats.png" >}}

</center>
