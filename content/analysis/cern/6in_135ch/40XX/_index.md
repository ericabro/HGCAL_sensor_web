---
title: "40XX"
date: 2018-08-15T14:06:34+02:00
draft: false
---

EOS path: Analysis_IVmeas/6in_135ch/40XX

The following folders are available:

{{%children %}}

<center>
## HexPlots
[all_means_summary.pdf](/images/Analysis_IVmeas/6in_135ch/40XX/all_means_summary.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/40XX/all_means_summary.png" caption="all_means_summary.png" >}}

[all_medians_summary.pdf](/images/Analysis_IVmeas/6in_135ch/40XX/all_medians_summary.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/40XX/all_medians_summary.png" caption="all_medians_summary.png" >}}

</center>
