---
title: "4004"
date: 2018-08-15T14:06:34+02:00
draft: false
---

EOS path: Analysis_IVmeas/6in_135ch/40XX/4004

The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_4004.pdf](/images/Analysis_IVmeas/6in_135ch/40XX/4004/IV_map_4004.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/40XX/4004/IV_map_4004.png" caption="IV_map_4004.png" >}}

[IV_map_4004_details_all_heat.pdf](/images/Analysis_IVmeas/6in_135ch/40XX/4004/IV_map_4004_details_all_heat.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/40XX/4004/IV_map_4004_details_all_heat.png" caption="IV_map_4004_details_all_heat.png" >}}

[IV_selCh_4004.pdf](/images/Analysis_IVmeas/6in_135ch/40XX/4004/IV_selCh_4004.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/40XX/4004/IV_selCh_4004.png" caption="IV_selCh_4004.png" >}}

[IV_selCh_4004_stats.pdf](/images/Analysis_IVmeas/6in_135ch/40XX/4004/IV_selCh_4004_stats.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/40XX/4004/IV_selCh_4004_stats.png" caption="IV_selCh_4004_stats.png" >}}

</center>
