---
title: "3007"
date: 2018-08-15T14:06:35+02:00
draft: false
---

EOS path: Analysis_IVmeas/6in_135ch/30XX/3007

The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_3007.pdf](/images/Analysis_IVmeas/6in_135ch/30XX/3007/IV_map_3007.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/30XX/3007/IV_map_3007.png" caption="IV_map_3007.png" >}}

[IV_map_3007_details_all_heat.pdf](/images/Analysis_IVmeas/6in_135ch/30XX/3007/IV_map_3007_details_all_heat.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/30XX/3007/IV_map_3007_details_all_heat.png" caption="IV_map_3007_details_all_heat.png" >}}

[IV_selCh_3007.pdf](/images/Analysis_IVmeas/6in_135ch/30XX/3007/IV_selCh_3007.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/30XX/3007/IV_selCh_3007.png" caption="IV_selCh_3007.png" >}}

[IV_selCh_3007_stats.pdf](/images/Analysis_IVmeas/6in_135ch/30XX/3007/IV_selCh_3007_stats.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/30XX/3007/IV_selCh_3007_stats.png" caption="IV_selCh_3007_stats.png" >}}

</center>
