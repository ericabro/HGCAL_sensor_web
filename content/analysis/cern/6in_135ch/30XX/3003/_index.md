---
title: "3003"
date: 2018-08-15T14:06:35+02:00
draft: false
---

EOS path: Analysis_IVmeas/6in_135ch/30XX/3003

The following folders are available:

{{%children %}}

<center>
## HexPlots
[IV_map_3003.pdf](/images/Analysis_IVmeas/6in_135ch/30XX/3003/IV_map_3003.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/30XX/3003/IV_map_3003.png" caption="IV_map_3003.png" >}}

[IV_map_3003_details_all_heat.pdf](/images/Analysis_IVmeas/6in_135ch/30XX/3003/IV_map_3003_details_all_heat.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/30XX/3003/IV_map_3003_details_all_heat.png" caption="IV_map_3003_details_all_heat.png" >}}

[IV_selCh_3003.pdf](/images/Analysis_IVmeas/6in_135ch/30XX/3003/IV_selCh_3003.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/30XX/3003/IV_selCh_3003.png" caption="IV_selCh_3003.png" >}}

[IV_selCh_3003_stats.pdf](/images/Analysis_IVmeas/6in_135ch/30XX/3003/IV_selCh_3003_stats.pdf)

{{< img src="/images/Analysis_IVmeas/6in_135ch/30XX/3003/IV_selCh_3003_stats.png" caption="IV_selCh_3003_stats.png" >}}

</center>
