---
title: "Z5471 N19"
date: 2019-06-07T10:53:35+02:00
draft: false
weight: 19
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[HPK_8in_198ch_120um_com_Z5471_19_IV.pdf](/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/HPK_8in_198ch_120um_com_Z5471_19_IV.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/HPK_8in_198ch_120um_com_Z5471_19_IV.png" caption="HPK_8in_198ch_120um_com_Z5471_19_IV.png" >}}

[HPK_8in_198ch_120um_com_Z5471_19_IV_selCh.pdf](/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/HPK_8in_198ch_120um_com_Z5471_19_IV_selCh.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/HPK_8in_198ch_120um_com_Z5471_19_IV_selCh.png" caption="HPK_8in_198ch_120um_com_Z5471_19_IV_selCh.png" >}}

[HPK_8in_198ch_120um_com_Z5471_19_IV_selCh_stats.pdf](/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/HPK_8in_198ch_120um_com_Z5471_19_IV_selCh_stats.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/HPK_8in_198ch_120um_com_Z5471_19_IV_selCh_stats.png" caption="HPK_8in_198ch_120um_com_Z5471_19_IV_selCh_stats.png" >}}

[HPK_8in_198ch_120um_com_Z5471_19_IV_totcurr.pdf](/images/Analysis_IVmeas/8in_192ch/120um/Z5471_19/HPK_8in_198ch_120um_com_Z5471_19_IV_totcurr.pdf)

</center>
