---
title: "Z5471 N21"
date: 2019-06-07T12:54:22+02:00
draft: false
weight: 21
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[HPK_8in_198ch_120um_com_Z5471_21_IV_-100V.pdf](/images/Analysis_IVmeas/8in_192ch/120um_new/Z5471_21/HPK_8in_198ch_120um_com_Z5471_21_IV_-100V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um_new/Z5471_21/HPK_8in_198ch_120um_com_Z5471_21_IV_-100V.png" caption="HPK_8in_198ch_120um_com_Z5471_21_IV_-100V.png" >}}

[HPK_8in_198ch_120um_com_Z5471_21_IV_-200V.pdf](/images/Analysis_IVmeas/8in_192ch/120um_new/Z5471_21/HPK_8in_198ch_120um_com_Z5471_21_IV_-200V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um_new/Z5471_21/HPK_8in_198ch_120um_com_Z5471_21_IV_-200V.png" caption="HPK_8in_198ch_120um_com_Z5471_21_IV_-200V.png" >}}

[HPK_8in_198ch_120um_com_Z5471_21_IV_-300V.pdf](/images/Analysis_IVmeas/8in_192ch/120um_new/Z5471_21/HPK_8in_198ch_120um_com_Z5471_21_IV_-300V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um_new/Z5471_21/HPK_8in_198ch_120um_com_Z5471_21_IV_-300V.png" caption="HPK_8in_198ch_120um_com_Z5471_21_IV_-300V.png" >}}

[HPK_8in_198ch_120um_com_Z5471_21_IV_selCh.pdf](/images/Analysis_IVmeas/8in_192ch/120um_new/Z5471_21/HPK_8in_198ch_120um_com_Z5471_21_IV_selCh.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um_new/Z5471_21/HPK_8in_198ch_120um_com_Z5471_21_IV_selCh.png" caption="HPK_8in_198ch_120um_com_Z5471_21_IV_selCh.png" >}}

[HPK_8in_198ch_120um_com_Z5471_21_IV_selCh_stats.pdf](/images/Analysis_IVmeas/8in_192ch/120um_new/Z5471_21/HPK_8in_198ch_120um_com_Z5471_21_IV_selCh_stats.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um_new/Z5471_21/HPK_8in_198ch_120um_com_Z5471_21_IV_selCh_stats.png" caption="HPK_8in_198ch_120um_com_Z5471_21_IV_selCh_stats.png" >}}

</center>
