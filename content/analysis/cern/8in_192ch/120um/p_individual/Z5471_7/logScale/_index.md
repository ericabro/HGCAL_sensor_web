---
title: "LogScale"
date: 2020-04-06T11:40:18+02:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[HPK_8in_198ch_120um_ind_Z5471_7_IV_-1000V_log.pdf](/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_-1000V_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_-1000V_log.png" caption="HPK_8in_198ch_120um_ind_Z5471_7_IV_-1000V_log.png" >}}

[HPK_8in_198ch_120um_ind_Z5471_7_IV_-100V_log.pdf](/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_-100V_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_-100V_log.png" caption="HPK_8in_198ch_120um_ind_Z5471_7_IV_-100V_log.png" >}}

[HPK_8in_198ch_120um_ind_Z5471_7_IV_-200V_log.pdf](/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_-200V_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_-200V_log.png" caption="HPK_8in_198ch_120um_ind_Z5471_7_IV_-200V_log.png" >}}

[HPK_8in_198ch_120um_ind_Z5471_7_IV_-300V_log.pdf](/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_-300V_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_-300V_log.png" caption="HPK_8in_198ch_120um_ind_Z5471_7_IV_-300V_log.png" >}}

[HPK_8in_198ch_120um_ind_Z5471_7_IV_-500V_log.pdf](/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_-500V_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_-500V_log.png" caption="HPK_8in_198ch_120um_ind_Z5471_7_IV_-500V_log.png" >}}

[HPK_8in_198ch_120um_ind_Z5471_7_IV_-600V_log.pdf](/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_-600V_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_-600V_log.png" caption="HPK_8in_198ch_120um_ind_Z5471_7_IV_-600V_log.png" >}}

[HPK_8in_198ch_120um_ind_Z5471_7_IV_-800V_log.pdf](/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_-800V_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_-800V_log.png" caption="HPK_8in_198ch_120um_ind_Z5471_7_IV_-800V_log.png" >}}

[HPK_8in_198ch_120um_ind_Z5471_7_IV_selCh_log.pdf](/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_selCh_log.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_selCh_log.png" caption="HPK_8in_198ch_120um_ind_Z5471_7_IV_selCh_log.png" >}}

[HPK_8in_198ch_120um_ind_Z5471_7_IV_selCh_log_stats.pdf](/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_selCh_log_stats.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_selCh_log_stats.png" caption="HPK_8in_198ch_120um_ind_Z5471_7_IV_selCh_log_stats.png" >}}

[HPK_8in_198ch_120um_ind_Z5471_7_IV_totcurr.pdf](/images/Analysis_IVmeas/8in_192ch/120um/p_ind/Z5471_7/logScale/HPK_8in_198ch_120um_ind_Z5471_7_IV_totcurr.pdf)

</center>
