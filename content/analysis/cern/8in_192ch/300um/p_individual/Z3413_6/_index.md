---
title: "Z3413_6"
date: 2019-06-07T12:54:22+02:00
draft: false
---


The following folders are available:

{{%children %}}

<center>
## HexPlots
[HPK_8in_198ch_300um_ind_Z3413_6_IV_-1000V.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_-1000V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_-1000V.png" caption="HPK_8in_198ch_300um_ind_Z3413_6_IV_-1000V.png" >}}

[HPK_8in_198ch_300um_ind_Z3413_6_IV_-100V.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_-100V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_-100V.png" caption="HPK_8in_198ch_300um_ind_Z3413_6_IV_-100V.png" >}}

[HPK_8in_198ch_300um_ind_Z3413_6_IV_-200V.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_-200V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_-200V.png" caption="HPK_8in_198ch_300um_ind_Z3413_6_IV_-200V.png" >}}

[HPK_8in_198ch_300um_ind_Z3413_6_IV_-300V.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_-300V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_-300V.png" caption="HPK_8in_198ch_300um_ind_Z3413_6_IV_-300V.png" >}}

[HPK_8in_198ch_300um_ind_Z3413_6_IV_-500V.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_-500V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_-500V.png" caption="HPK_8in_198ch_300um_ind_Z3413_6_IV_-500V.png" >}}

[HPK_8in_198ch_300um_ind_Z3413_6_IV_-800V.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_-800V.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_-800V.png" caption="HPK_8in_198ch_300um_ind_Z3413_6_IV_-800V.png" >}}

[HPK_8in_198ch_300um_ind_Z3413_6_IV_selCh.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_selCh.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_selCh.png" caption="HPK_8in_198ch_300um_ind_Z3413_6_IV_selCh.png" >}}

[HPK_8in_198ch_300um_ind_Z3413_6_IV_selCh_stats.pdf](/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_selCh_stats.pdf)

{{< img src="/images/Analysis_IVmeas/8in_192ch/300um/p_individual/Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV_selCh_stats.png" caption="HPK_8in_198ch_300um_ind_Z3413_6_IV_selCh_stats.png" >}}

</center>
