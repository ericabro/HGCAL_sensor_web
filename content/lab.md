---
title: Lab pictures
disableToc: true
---

<center>

## Sensors and setup

{{< img src="/images/lab/probestation_complete.jpg?width=60pc" caption="Probestation with probe card with integrated switching matrix (setup for 8 inches 192 cells sensors)." >}}

{{< img src="/images/lab/microscope.jpg?width=60pc" caption="Microscope to contact guard ring." >}}

{{< img src="/images/lab/6_inch_135_cell_Hexaboard_probe_card_setup.JPG?width=60pc" caption="Hexaboard probe card setup for 6 inches 135 cells sensors." >}}

{{< img src="/images/lab/6_inch_135_cell_Needle_measurements2.jpg?width=60pc" caption="Seven needle measurement setup for 6 inches 135 cells sensors." >}}

{{< img src="/images/lab/6_inch_135_cell_Needle_measurements.jpg?width=60pc" caption="Zoom on the sensor for the Seven needle measurement." >}}

{{< img src="/images/lab/6_inch_135_cell_Probecard.JPG?width=60pc" caption="Probe card for 6 inches 135 cells sensors." >}}

{{< img src="/images/lab/8_inch_271_cell_Setup.JPG?width=60pc" caption="Hexaboard probe card setup for 8 inches 271 cells sensors (sensor in)." >}}

{{< img src="/images/lab/8_inch_271_cell_Setup2.JPG?width=60pc" caption="Hexaboard probe card setup for 8 inches 271 cells sensors (sensor out)." >}}

{{< img src="/images/lab/8_inch_271_cell_Setup3.JPG?width=40pc" caption="Hexaboard probe card setup for 8 inches 271 cells sensors during alignment procedure." >}}

## Lab

{{< img src="/images/lab/lab.jpg?width=60pc" caption="Laboratoire LCD at CERN, building 186, 1-203." >}}

## Shifters

{{< img src="/images/lab/mateus.jpg?width=60pc" caption="Mateus is inspecting the sensor under the microscope. Is this a bit of dust?!">}}

{{< img src="/images/lab/erica.jpg?width=60pc" caption="Erica is picking up the sensor from its box.">}}

{{< img src="/images/lab/erica&eva.jpg?width=60pc" caption="Eva and Erica are moving the sensor in the probe station with a vacuum tool.">}}

{{< img src="/images/lab/Eva.jpg?width=60pc" caption="The sensors are very fragile! Eva is moving the sensor in the measurement setup carefully." >}}

{{< img src="/images/lab/Thorben.jpg?width=25pc" caption="Thorben is positioning the sensor on the chuck.">}}

{{< img src="/images/lab/pedro.jpg?width=60pc" caption="Pedro is performing the alignment of the sensor before contact: a new measurement is about to start!">}}

{{< img src="/images/lab/mateus_gelpack.jpg?width=60pc" caption="Mateus is packing up the sensor in a GelBox. It is time for it to be tested in another lab.">}}

## Ex-Shifters

{{< img src="/images/lab/Flo.jpg?width=25pc" caption="Flo looking \"into\" the sensor." >}}

{{< img src="/images/lab/andreas.jpg?width=60pc" caption="Andreas is handling test structures." >}}

{{< img src="/images/lab/pieter.jpg?width=35pc" caption="Pieter is setting up a 7-needle measurement." >}}


</center>
