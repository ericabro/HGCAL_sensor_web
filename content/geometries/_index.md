---
title: "Geometries"
weight: 10
draft: false
---

This page collects the geometry of the sensors analyzed in the lab (produced using [HexPlot](https://gitlab.cern.ch/CLICdp/HGCAL/HGCAL_sensor_analysis)).

<center>
## HPK 6inches 135ch

{{< img src="/images/geometries/geometry_HPK_6inch_135ch.png" caption="" >}}

[Geometry in pdf](/images/geometries/geometry_HPK_6inch_135ch.pdf)

## HPK 6inches 239ch

{{< img src="/images/geometries/geometry_HPK_6inch_239ch.png" caption="" >}}

[Geometry in pdf](/images/geometries/geometry_HPK_6inch_239ch.pdf)

## HPK 8inches 271ch

{{< img src="/images/geometries/geometry_HPK_8inch_271ch.png" caption="" >}}

[Geometry in pdf](/images/geometries/geometry_HPK_8inch_271ch.pdf)

## HPK 8inches 192ch (2018 layout)

{{< img src="/images/geometries/geometry_HPK_8inch_198ch.png" caption="" >}}

[Geometry in pdf](/images/geometries/geometry_HPK_8inch_198ch.pdf)

## HPK 8inches 192ch (2019 layout)

{{< img src="/images/geometries/geometry_HPK_8inch_192ch_edge_ring.png" caption="" >}}

[Geometry in pdf](/images/geometries/geometry_HPK_8inch_192ch_edge_ring.pdf)

## HPK 8inches 432ch (2019 layout)

{{< img src="/images/geometries/geometry_HPK_8inch_432ch_edge_ring.png" caption="" >}}

[Geometry in pdf](/images/geometries/geometry_HPK_8inch_432ch_edge_ring.pdf)

</center>
