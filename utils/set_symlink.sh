#!/bin/bash
TARGET="/eos/user/h/hgsensor/HGCAL_test_results/Results/"
RESULTS=Results
if [ ! -d "$RESULTS" ]; then
	echo "Setting symlink to $TARGET"
	ln -s "$TARGET" Results
else
	echo "Link already exists, skip!"
fi