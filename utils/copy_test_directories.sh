#!/bin/bash
SOURCE="/eos/user/h/hgsensor/HGCAL_test_results/Results/"
TARGET=Results

cp -r $SOURCE/Full_Matrix_Custom0/ $TARGET
cp -r $SOURCE/HPK_6in_135ch_1009 $TARGET
cp -r $SOURCE/HPK_6in_135ch_1105 $TARGET
cp -r $SOURCE/HPK_6in_135ch_1102_Needle $TARGET
cp -r $SOURCE/HPK_6in_135ch_4002 $TARGET
cp -r $SOURCE/HPK_6in_135ch_4002_Needle $TARGET
cp -r $SOURCE/HPK_8in_271ch_Freesample_1 $TARGET
cp -r $SOURCE/HPK_8in_271ch_Freesample_1_Needle $TARGET
cp -r $SOURCE/HPK_8in_271ch_Correction_Open $TARGET
cp -r $SOURCE/HPK_8in_271ch_Correction_Short $TARGET