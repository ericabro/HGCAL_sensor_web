#!/bin/bash
echo "Setting python environment..."
export PYTHONDIR=/cvmfs/clicdp.cern.ch/software/Python/2.7.12/x86_64-slc6-gcc62-opt
export PATH=$PYTHONDIR/bin:$PATH
export LD_LIBRARY_PATH=$PYTHONDIR/lib:$LD_LIBRARY_PATH
# export PYTHONPATH=/afs/cern.ch/user/a/amaier/.local/lib/python3.5/site-packages/
# export PATH=/afs/cern.ch/user/a/amaier/.local/bin:$PATH
