#!/usr/bin/python


import argparse
import sys
import os
import shutil
import fileinput

from src import html_helper
from src import file_walker
from shutil import copytree

DATA_PATH = 'Analysis_IVmeas/'
HUGO_ANALYSIS_DIR = 'new_analysis/'
SHOW_NOT_FOUND = True


def test_category(name, cat, catList=[]):
    if (name.startswith(cat)):
        for item in catList:
            if item == cat:
                continue
            if (name.startswith(item)) and len(item) > len(cat):
                return False
        return True
    return False

def displayFigure(name, caption, filename):
    printFig = "{{< img src=\"/images/" + name + "\" caption=\"" + caption + "\" >}}"
    print(printFig, file=filename)
    print('', file=filename)
    return True

def displayPDF(name, caption, filename):
    printCapt = "[" + caption + "](/images/" + name + ")"
    print(printCapt, file=filename)
    print('', file=filename)
    return True

dataDirs, dataCategories, dataSubDirContent = file_walker.walk_dirs(DATA_PATH)

#commented line to not create the index of analysis anymore
HugoCommandNewFile = "hugo new " + HUGO_ANALYSIS_DIR + "_index.md" 
os.system(HugoCommandNewFile)

mainDir = 'content/' + HUGO_ANALYSIS_DIR
listName = mainDir + '_index.md'
print(listName)
with open(listName, 'a') as myfile:
    print('The following sensor comparison and summary are available:', file=myfile)
    print('', file=myfile)
    print('{{%children %}}', file=myfile)

for dataSubDir, dataSubDirName, dataSubFileList in dataSubDirContent:
    #print(dataSubDir)
    HugoCommandNewDataFile = "hugo new " + HUGO_ANALYSIS_DIR + dataSubDir + "/_index.md" 
    #print(HugoCommandNewDataFile)
    os.system(HugoCommandNewDataFile)
    dataName = mainDir + dataSubDir + "/_index.md"
    with open(dataName, 'a') as myfile:
        print('', file=myfile)
        print('The following folders are available:', file=myfile)
        print('', file=myfile)
        print('{{%children %}}', file=myfile)
        print('', file=myfile)

        print('Now create the single figures ...')
        #IV and CV figures
        hexPlots = [plot for plot in dataSubFileList
                    if '.png' in plot or '.pdf' in plot]
        hexPlots = sorted(hexPlots)
        #print(hexPlots)
        print("<center>", file=myfile)
        if hexPlots or SHOW_NOT_FOUND:
            print('## HexPlots', file=myfile)
            if not hexPlots:
                print("No IV or CV plots found in this folder.", file=myfile)
                continue

            srcpath = os.path.dirname(plot)
            dstpath = "static/images/"+srcpath
            #ERICA:: or replace it?
            if not os.path.isdir(dstpath) :
                shutil.copytree(srcpath, dstpath)

            for plot in hexPlots:
                srcpath, srcfile = os.path.split(plot)
                #os.system(commandCopy)
                if '.png' in plot:
                    displayFigure(plot, srcfile, myfile)
                if '.pdf' in plot:
                    displayPDF(plot, srcfile, myfile)

        print("</center>", file=myfile)

